/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.odisee.vanden_bossche_jasper_spellen.dataaccess;
import be.odisee.vanden_bossche_jasper_spellen.beans.Soort;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
/**
 *
 * @author JVDBG19
 */
public class DASoort {
    private static final String URL = "jdbc:oracle:thin:@localhost:1521:XE";
    private static final String LOGIN = "admin";
    private static final String PASSWORD = "sql";
    private static final String DRIVER = "oracle.jdbc.driver.OracleDriver";

    private Connection createConnection() throws SQLException {
        return DriverManager.getConnection(URL, LOGIN, PASSWORD);
    }
    // case 1
    public Soort findSoort() {
        try (Connection connection = createConnection()) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM soort WHERE nr = 1");
            if (resultSet.next()) {
                Soort soort = new Soort();
                soort.setNr(resultSet.getInt("nr"));
                soort.setSoortnaam(resultSet.getString("soortnaam"));
                
                return soort;
           } else{
               return null;
           }       
            
        }catch(SQLException e){
            throw new SoortException("Unable to retrieve data from the database", e);
        }
    }
}
               

