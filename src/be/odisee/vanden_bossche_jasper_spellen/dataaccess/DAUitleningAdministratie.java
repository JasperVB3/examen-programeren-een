/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.odisee.vanden_bossche_jasper_spellen.dataaccess;

import be.odisee.vanden_bossche_jasper_spellen.beans.Uitlening;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
/**
 *
 * @author JVDBG19
 */
public class DAUitleningAdministratie {
    
    private static final String URL = "jdbc:oracle:thin:@localhost:1521:XE";
    private static final String LOGIN = "admin";
    private static final String PASSWORD = "sql";
    private static final String DRIVER = "oracle.jdbc.driver.OracleDRIVER";

    private Connection createConnection() throws SQLException {
        return DriverManager.getConnection(URL, LOGIN, PASSWORD);

    }
    
    private static java.sql.Date getCurrentDate() {
    java.util.Date today = new java.util.Date();
    return new java.sql.Date(today.getTime());
     }
    // case 11 database aanpassen
    public Uitlening addUitleningAdministratie(int spelNr,int lenerNR){
        Uitlening uitlening = new Uitlening();
        try(Connection connection = createConnection())
        {
            PreparedStatement pstatement = connection.prepareStatement("INSERT INTO uitlening VALUES (uitlening_seq.nextval,?,?,?,null)");
            pstatement.setInt(1,spelNr);
            pstatement.setInt(2,lenerNR);
            pstatement.setDate(3, getCurrentDate());
            pstatement.executeUpdate();
           
        }catch(SQLException e){
            throw new SoortException("Unable to retrieve data from the database", e);
        }
        return uitlening;
    }
    
    
    
    
}