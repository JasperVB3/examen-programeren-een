/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.odisee.vanden_bossche_jasper_spellen.dataaccess;

import be.odisee.vanden_bossche_jasper_spellen.beans.Lener;
import be.odisee.vanden_bossche_jasper_spellen.beans.Spel;
import be.odisee.vanden_bossche_jasper_spellen.beans.Uitlening;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author JVDBG19
 */

public class DAUitlening {
      private static final String URL = "jdbc:oracle:thin:@localhost:1521:XE";
    private static final String LOGIN = "admin";
    private static final String PASSWORD = "sql";
    private static final String DRIVER = "oracle.jdbc.driver.OracleDriver";

    private Connection createConnection() throws SQLException {
        return DriverManager.getConnection(URL, LOGIN, PASSWORD);
    }
    // case 7 toon alle uitleningen
    public ArrayList <Uitlening> allUitleningen() {
    ArrayList<Uitlening> uitleningen = new ArrayList<>();
    try (Connection connection = createConnection()) {
        PreparedStatement pstatement = connection.prepareStatement("SELECT * FROM uitlening INNER JOIN spel on uitlening.spelnr = spel.nr inner join lener on uitlening.lenernr = lener.nr order by lener.lenernaam, uitlening.uitleendatum");
        ResultSet resultSet = pstatement.executeQuery();
    while (resultSet.next()){
        
        Uitlening uitlening = new Uitlening();
        uitlening.setUitleendatum(resultSet.getDate("uitleendatum"));
        uitlening.setTerugbrengdatum(resultSet.getDate("terugbrengdatum"));
        uitlening.setSpelnr(resultSet.getInt("spelnr"));
        uitlening.setLenernr(resultSet.getInt("lenernr"));
        
        
        Spel spel = new Spel();
        spel.setNaam(resultSet.getString("naam"));
        spel.setNr(resultSet.getInt("nr"));
        
        uitlening.setSpel(spel);
        
        Lener lener = new Lener();
        lener.setLenernaam(resultSet.getString("lenernaam"));
        lener.setSpelnr(resultSet.getInt("spelnr"));
        lener.setNr(resultSet.getInt("nr"));
        
        uitlening.setLener(lener);
        
        uitleningen.add(uitlening);
        
    }return uitleningen;
   
    }catch(SQLException e){
            throw new SoortException("Unable to retrieve data from the database", e);
        }
     }
    
    // case 7 toon opgevraagde uitleningen
    public ArrayList <Uitlening> allUitleningen2(String userInput3) {
    ArrayList<Uitlening> uitleningen = new ArrayList<>();
    try (Connection connection = createConnection()) {
        PreparedStatement pstatement = connection.prepareStatement("SELECT * FROM uitlening INNER JOIN spel on uitlening.spelnr = spel.nr inner join lener on uitlening.lenernr = lener.nr where lower(lener.lenernaam) like ? order by lener.lenernaam");
        pstatement.setString(1, "%"+ userInput3 + "%");
        ResultSet resultSet = pstatement.executeQuery();
    while (resultSet.next()){
        
        Uitlening uitlening = new Uitlening();
        uitlening.setUitleendatum(resultSet.getDate("uitleendatum"));
        uitlening.setTerugbrengdatum(resultSet.getDate("terugbrengdatum"));
        uitlening.setSpelnr(resultSet.getInt("spelnr"));
        uitlening.setLenernr(resultSet.getInt("lenernr"));
        
        
        Spel spel = new Spel();
        spel.setNaam(resultSet.getString("naam"));
        spel.setNr(resultSet.getInt("nr"));
        
        uitlening.setSpel(spel);
        
        Lener lener = new Lener();
        lener.setLenernaam(resultSet.getString("lenernaam"));
        lener.setSpelnr(resultSet.getInt("spelnr"));
        lener.setNr(resultSet.getInt("nr"));
        
        uitlening.setLener(lener);
        
        uitleningen.add(uitlening);
        
    }return uitleningen;
    
    
   
    }catch(SQLException e){
            throw new SoortException("Unable to retrieve data from the database", e);
        }
     }   
    // case 10 toon alle uitleningen die niet zijn teruggebracht
    public ArrayList <Uitlening> allUitleningen3() {
    ArrayList<Uitlening> uitleningen = new ArrayList<>();
    try (Connection connection = createConnection()) {
        PreparedStatement pstatement = connection.prepareStatement("SELECT * FROM uitlening INNER JOIN spel on uitlening.spelnr = spel.nr inner join lener on uitlening.lenernr = lener.nr where terugbrengdatum is null order by spel.naam");
        ResultSet resultSet = pstatement.executeQuery();
    while (resultSet.next()){
        
        Uitlening uitlening = new Uitlening();
        uitlening.setUitleendatum(resultSet.getDate("uitleendatum"));
        uitlening.setTerugbrengdatum(resultSet.getDate("terugbrengdatum"));
        uitlening.setSpelnr(resultSet.getInt("spelnr"));
        uitlening.setLenernr(resultSet.getInt("lenernr"));
        
        
        Spel spel = new Spel();
        spel.setNaam(resultSet.getString("naam"));
        spel.setNr(resultSet.getInt("nr"));
        
        uitlening.setSpel(spel);
        
        Lener lener = new Lener();
        lener.setLenernaam(resultSet.getString("lenernaam"));
        lener.setSpelnr(resultSet.getInt("spelnr"));
        lener.setNr(resultSet.getInt("nr"));
        
        uitlening.setLener(lener);
        
        uitleningen.add(uitlening);
        
    }return uitleningen;
    
    
   
    }catch(SQLException e){
            throw new SoortException("Unable to retrieve data from the database", e);
        }
     }   
   // case 11 alle uitleningen 
    public ArrayList <Spel> allUitleningen4() {
    ArrayList<Spel> uitleningen = new ArrayList<>();
    try (Connection connection = createConnection()) {
        PreparedStatement pstatement = connection.prepareStatement("select LENERNAAM,SPEL.NAAM,UITLEENDATUM,TERUGBRENGDATUM,SPELNR,LENERNR from spel inner join uitlening on UITLENING.SPELNR = spel.NR inner join lener on lener.NR = UITLENING.LENERNR order by uitleendatum,terugbrengdatum");
            ResultSet resultSet = pstatement.executeQuery();
    while (resultSet.next()){
        
                Spel spel8 = new Spel();
                spel8.setNaam(resultSet.getString("Naam"));
                Lener lener = new Lener();
                lener.setLenernaam(resultSet.getString("Lenernaam"));
                spel8.setLener(lener);
                Uitlening uitlening = new Uitlening();
                uitlening.setSpelnr(resultSet.getInt("Spelnr"));
                uitlening.setLenernr(resultSet.getInt("lenernr"));
                uitlening.setUitleendatum(resultSet.getDate("UitleenDatum"));
                uitlening.setTerugbrengdatum(resultSet.getDate("TerugbrengDatum"));
                spel8.setUitlening(uitlening);                             
                uitleningen.add(spel8);
        
    }return uitleningen;
    
    
   
    }catch(SQLException e){
            throw new SoortException("Unable to retrieve data from the database", e);
        }
     }   
    
    
}
