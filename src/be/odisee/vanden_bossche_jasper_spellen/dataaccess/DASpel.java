/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.odisee.vanden_bossche_jasper_spellen.dataaccess;


import be.odisee.vanden_bossche_jasper_spellen.beans.Moeilijkheid;
import be.odisee.vanden_bossche_jasper_spellen.beans.Soort;
import be.odisee.vanden_bossche_jasper_spellen.beans.Spel;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author JVDBG19
 */
public class DASpel {
      private static final String URL = "jdbc:oracle:thin:@localhost:1521:XE";
    private static final String LOGIN = "admin";
    private static final String PASSWORD = "sql";
    private static final String DRIVER = "oracle.jdbc.driver.OracleDriver";

    private Connection createConnection() throws SQLException {
        return DriverManager.getConnection(URL, LOGIN, PASSWORD);
    }
    // case 2
    public Spel findSpel() {
        try (Connection connection = createConnection()) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM spel WHERE nr = 5");
            if (resultSet.next()) {
                Spel spel = new Spel();
                spel.setNr(resultSet.getInt("nr"));
                spel.setNaam(resultSet.getString("naam"));
                spel.setUitgever(resultSet.getString("uitgever"));
                spel.setAuteur(resultSet.getString("auteur"));
                spel.setJaar_uitgifte(resultSet.getInt("jaar_uitgifte"));
                spel.setLeeftijd(resultSet.getString("leeftijd"));
                spel.setMin_spelers(resultSet.getInt("min_spelers"));
                spel.setMax_spelers(resultSet.getInt("max_spelers"));
                spel.setSoortnr(resultSet.getInt("soortnr"));
                spel.setSpeelduur(resultSet.getString("speelduur"));
                spel.setMoeilijkheidnr(resultSet.getInt("moeilijkheidnr"));
                spel.setPrijs(resultSet.getDouble("prijs"));
                spel.setAfbeelding(resultSet.getString("afbeelding"));
                
                
                return spel;
           } else{
               return null;
           }       
            
        }catch(SQLException e){
            throw new SoortException("Unable to retrieve data from the database", e);
        }
    }
    // case 4
     public Spel searchSpel(String userInput){
        try (Connection connection = createConnection()){
            PreparedStatement pstatement = connection.prepareStatement("select * from spel where lower(SPEL.naam) like ?");
            pstatement.setString(1, "%"+ userInput + "%");
            ResultSet resultSet = pstatement.executeQuery();
            
           if (resultSet.next()){
               
               Spel spel = new Spel();
            
                spel.setNr(resultSet.getInt("nr"));
                spel.setNaam(resultSet.getString("naam"));
                spel.setUitgever(resultSet.getString("uitgever"));
                spel.setAuteur(resultSet.getString("auteur"));
                spel.setJaar_uitgifte(resultSet.getInt("jaar_uitgifte"));
                spel.setLeeftijd(resultSet.getString("leeftijd"));
                spel.setMin_spelers(resultSet.getInt("min_spelers"));
                spel.setMax_spelers(resultSet.getInt("max_spelers"));
                spel.setSoortnr(resultSet.getInt("soortnr"));
                spel.setSpeelduur(resultSet.getString("speelduur"));
                spel.setMoeilijkheidnr(resultSet.getInt("moeilijkheidnr"));
                spel.setPrijs(resultSet.getDouble("prijs"));
                spel.setAfbeelding(resultSet.getString("afbeelding"));
                return spel;
           
           }
           else{
               return null;
                  
           }
         }
        catch(SQLException e){
            throw new SoortException("Unable to retrieve data from the database", e);
        }
     }
    
     // case 5
     public ArrayList<Spel> allSpellen() {
    ArrayList<Spel> spellen = new ArrayList<>();
    try (Connection connection = createConnection()) {
        PreparedStatement pstatement = connection.prepareStatement("SELECT * FROM spel order by spel.naam");
        ResultSet resultSet = pstatement.executeQuery();
    while (resultSet.next()){
        Spel spel = new Spel();
        spel.setNr(resultSet.getInt("nr"));
        spel.setNaam(resultSet.getString("naam"));
        spel.setUitgever(resultSet.getString("uitgever"));
        spel.setPrijs(resultSet.getDouble("prijs"));
        
        
        
        spellen.add(spel);
        
    }return spellen;
   
    }catch(SQLException e){
            throw new SoortException("Unable to retrieve data from the database", e);
        }
     }
     
     // case 6
     public ArrayList<Spel> allSpellen2() {
    ArrayList<Spel> spellen = new ArrayList<>();
    try (Connection connection = createConnection()) {
        PreparedStatement pstatement = connection.prepareStatement("SELECT * FROM spel INNER JOIN soort on spel.soortnr = soort.nr  order by spel.naam");
        ResultSet resultSet = pstatement.executeQuery();
    while (resultSet.next()){
        Spel spel = new Spel();
        spel.setNaam(resultSet.getString("naam"));
        
        Soort soort = new Soort();
        soort.setSoortnaam(resultSet.getString("soortnaam"));
        
        spel.setSoort(soort);
       
        spellen.add(spel);
        
    }return spellen;
   
    }catch(SQLException e){
            throw new SoortException("Unable to retrieve data from the database", e);
        }
     }
     // case 6
     public Spel searchSpel2(String userInput3){
        try (Connection connection = createConnection()){
            PreparedStatement pstatement = connection.prepareStatement("select * from spel INNER JOIN soort on spel.soortnr = soort.nr INNER JOIN moeilijkheid on spel.moeilijkheidnr = moeilijkheid.nr where lower(SPEL.naam) like ?");
            pstatement.setString(1, "%"+ userInput3 + "%");
            ResultSet resultSet = pstatement.executeQuery();
            
           if (resultSet.next()){
               
               Spel spel = new Spel();
                spel.setNr(resultSet.getInt("nr"));
                spel.setNaam(resultSet.getString("naam"));
                spel.setUitgever(resultSet.getString("uitgever"));
                spel.setAuteur(resultSet.getString("auteur"));
                spel.setJaar_uitgifte(resultSet.getInt("jaar_uitgifte"));
                spel.setLeeftijd(resultSet.getString("leeftijd"));
                spel.setMin_spelers(resultSet.getInt("min_spelers"));
                spel.setMax_spelers(resultSet.getInt("max_spelers"));
                spel.setSoortnr(resultSet.getInt("soortnr"));
                spel.setSpeelduur(resultSet.getString("speelduur"));
                spel.setMoeilijkheidnr(resultSet.getInt("moeilijkheidnr"));
                spel.setPrijs(resultSet.getDouble("prijs"));
                spel.setAfbeelding(resultSet.getString("afbeelding"));
                
                Soort soort = new Soort();
                soort.setNr(resultSet.getInt("nr"));
                soort.setSoortnaam(resultSet.getString("soortnaam"));
                
                spel.setSoort(soort);
                
                Moeilijkheid moeilijkheid = new Moeilijkheid();
                moeilijkheid.setNr(resultSet.getInt("nr"));
                moeilijkheid.setMoeilijkheidnaam(resultSet.getString("moeilijkheidnaam"));
                
                spel.setMoeilijkheid(moeilijkheid);
                
                return spel;
           
           }
           else{
               return null;
                  
           }
         }

 catch(SQLException e){
            throw new SoortException("Unable to retrieve data from the database", e);
        }
     }
     
// case 11 toon het opgevraagde spel
public Spel searchSpel3(int userInput3) {
    
    try (Connection connection = createConnection()) {
        PreparedStatement pstatement = connection.prepareStatement("SELECT * FROM SPEL where spel.nr like ?");
        pstatement.setInt(1 , userInput3);
        ResultSet resultSet = pstatement.executeQuery();
    if (resultSet.next()){
               
               Spel spel = new Spel();
                spel.setNr(resultSet.getInt("nr"));
                spel.setNaam(resultSet.getString("naam"));
                spel.setLeeftijd(resultSet.getString("leeftijd"));
                spel.setMin_spelers(resultSet.getInt("min_spelers"));
                spel.setMax_spelers(resultSet.getInt("max_spelers"));
                spel.setSpeelduur(resultSet.getString("speelduur"));
                spel.setPrijs(resultSet.getDouble("prijs"));
                spel.setAfbeelding(resultSet.getString("afbeelding"));
                
                
                
                
                
                
                return spel;
           
           }
           else{
               return null;
                  
           }
         }
   
    catch(SQLException e){
            throw new SoortException("Unable to retrieve data from the database", e);
        }
     }
 // case 11 toon alle spellen 
public ArrayList<Spel> allSpellen3() {
    ArrayList<Spel> spellen = new ArrayList<>();
    try (Connection connection = createConnection()) {
        PreparedStatement pstatement = connection.prepareStatement("SELECT * FROM spel order by spel.nr");
        ResultSet resultSet = pstatement.executeQuery();
    while (resultSet.next()){
        Spel spel = new Spel();
        spel.setNr(resultSet.getInt("nr"));
        spel.setNaam(resultSet.getString("naam"));
        spel.setUitgever(resultSet.getString("uitgever"));
        spel.setPrijs(resultSet.getDouble("prijs"));
        
        
        
        spellen.add(spel);
        
    }return spellen;
   
    }catch(SQLException e){
            throw new SoortException("Unable to retrieve data from the database", e);
        }
     }
}


     



