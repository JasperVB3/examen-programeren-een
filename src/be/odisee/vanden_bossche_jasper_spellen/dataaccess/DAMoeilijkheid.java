/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.odisee.vanden_bossche_jasper_spellen.dataaccess;

import be.odisee.vanden_bossche_jasper_spellen.beans.Moeilijkheid;
import be.odisee.vanden_bossche_jasper_spellen.beans.Spel;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author JVDBG19
 */
public class DAMoeilijkheid {
    private static final String URL = "jdbc:oracle:thin:@localhost:1521:XE";
    private static final String LOGIN = "admin";
    private static final String PASSWORD = "sql";
    private static final String DRIVER = "oracle.jdbc.driver.OracleDriver";

    private Connection createConnection() throws SQLException {
        return DriverManager.getConnection(URL, LOGIN, PASSWORD);
    }
    // case 8 toon alle moeilijkheden
    public ArrayList <Moeilijkheid> allMoeilijkheid() {
    ArrayList<Moeilijkheid> moeilijkheden = new ArrayList<>();
    try (Connection connection = createConnection()) {
        PreparedStatement pstatement = connection.prepareStatement("SELECT * FROM moeilijkheid");
        ResultSet resultSet = pstatement.executeQuery();
    while (resultSet.next()){
        
        Moeilijkheid moeilijkheid = new Moeilijkheid();
        moeilijkheid.setNr(resultSet.getInt("nr"));
        moeilijkheid.setMoeilijkheidnaam(resultSet.getString("moeilijkheidnaam"));
        
        moeilijkheden.add(moeilijkheid);
        
    }return moeilijkheden;
   
    }catch(SQLException e){
            throw new SoortException("Unable to retrieve data from the database", e);
        }
     }
    // case 8 toon de spellen die voldoen aan de geselcteerde moeilijkheden.
    public ArrayList <Moeilijkheid> allMoeilijkheid2(int userInput1) {
    ArrayList<Moeilijkheid> moeilijkheden = new ArrayList<>();
    try (Connection connection = createConnection()) {
        PreparedStatement pstatement = connection.prepareStatement("SELECT * FROM moeilijkheid INNER JOIN spel on spel.moeilijkheidnr = moeilijkheid.nr where moeilijkheid.nr >= ? order by spel.naam");
        pstatement.setInt(1, userInput1);
        ResultSet resultSet = pstatement.executeQuery();
    while (resultSet.next()){
        
        Moeilijkheid moeilijkheid = new Moeilijkheid();
        moeilijkheid.setNr(resultSet.getInt("nr"));
        moeilijkheid.setMoeilijkheidnaam(resultSet.getString("moeilijkheidnaam"));
        
        Spel spel = new Spel();
        spel.setNr(resultSet.getInt("nr"));
        spel.setNaam(resultSet.getString("naam"));
        spel.setUitgever(resultSet.getString("uitgever"));
        spel.setAuteur(resultSet.getString("auteur"));
        spel.setJaar_uitgifte(resultSet.getInt("jaar_uitgifte"));
        spel.setLeeftijd(resultSet.getString("leeftijd"));
        spel.setMin_spelers(resultSet.getInt("min_spelers"));
        spel.setMax_spelers(resultSet.getInt("max_spelers"));
        spel.setSoortnr(resultSet.getInt("soortnr"));
        spel.setSpeelduur(resultSet.getString("speelduur"));
        spel.setMoeilijkheidnr(resultSet.getInt("moeilijkheidnr"));
        spel.setPrijs(resultSet.getDouble("prijs"));
        spel.setAfbeelding(resultSet.getString("afbeelding"));
        
        moeilijkheid.setSpel(spel);
       
        
        moeilijkheden.add(moeilijkheid);
        
    }return moeilijkheden;
   
    }catch(SQLException e){
            throw new SoortException("Unable to retrieve data from the database", e);
        }
     }
}
