/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.odisee.vanden_bossche_jasper_spellen.dataaccess;

import be.odisee.vanden_bossche_jasper_spellen.beans.Lener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author JVDBG19
 */
public class DALener {
     private static final String URL = "jdbc:oracle:thin:@localhost:1521:XE";
    private static final String LOGIN = "admin";
    private static final String PASSWORD = "sql";
    private static final String DRIVER = "oracle.jdbc.driver.OracleDriver";

    private Connection createConnection() throws SQLException {
        return DriverManager.getConnection(URL, LOGIN, PASSWORD);
    }
    // case 3
    public Lener findLener() {
        try (Connection connection = createConnection()) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM lener WHERE nr = 1");
            if (resultSet.next()) {
                Lener lener = new Lener();
                lener.setLenernaam(resultSet.getString("lenernaam"));
                lener.setGemeente(resultSet.getString("gemeente"));
                
                return lener;
           } else{
               return null;
           }       
            
        }catch(SQLException e){
            throw new SoortException("Unable to retrieve data from the database", e);
        }
    
    }
     // case 7 en case 11
     public ArrayList <Lener> allLener() {
    ArrayList<Lener> leners = new ArrayList<>();
    try (Connection connection = createConnection()) {
        PreparedStatement pstatement = connection.prepareStatement("SELECT * FROM lener");
        ResultSet resultSet = pstatement.executeQuery();
    while (resultSet.next()){
        
        Lener lener = new Lener();
        lener.setLenernaam(resultSet.getString("lenernaam"));
        lener.setNr(resultSet.getInt("nr"));
        
        leners.add(lener);
        
    }return leners;
   
    }catch(SQLException e){
            throw new SoortException("Unable to retrieve data from the database", e);
        }
     }
     // case 9 toon de leners die voldoen aan de input
     public ArrayList <Lener> allLener2(String userInput) {
    ArrayList<Lener> leners = new ArrayList<>();
    try (Connection connection = createConnection()) {
        PreparedStatement pstatement = connection.prepareStatement("SELECT * FROM lener where lower(lener.lenernaam) like ? order by lener.lenernaam");
        pstatement.setString(1, "%"+ userInput + "%");
        ResultSet resultSet = pstatement.executeQuery();
    while (resultSet.next()){
        
        Lener lener = new Lener();
        lener.setLenernaam(resultSet.getString("lenernaam"));
        lener.setGemeente(resultSet.getString("gemeente"));
        lener.setTelefoon(resultSet.getString("telefoon"));
        lener.setEmail(resultSet.getString("email"));
        
        leners.add(lener);
        
    }return leners;
   
    }catch(SQLException e){
            throw new SoortException("Unable to retrieve data from the database", e);
        }
     }
     // case 11 
     public Lener searchLener(int userinput){
        try(Connection connection = createConnection())
        {
            PreparedStatement pstatement = connection.prepareStatement("select LENERNAAM,GEMEENTE,TELEFOON,EMAIL from lener where lener.nr like ?");
            pstatement.setInt(1, userinput);
            ResultSet resultSet = pstatement.executeQuery();
            
            if(resultSet.next())
            {
                Lener lener = new Lener();
                lener.setLenernaam(resultSet.getString("Lenernaam"));
                lener.setGemeente(resultSet.getString("gemeente"));
                lener.setTelefoon(resultSet.getString("telefoon"));
                lener.setEmail(resultSet.getString("email"));
                return lener; 
                            
                
                                            
            }
            else
            {
                return null;
            }
         
        }
        catch(SQLException e)
        {
            throw new SoortException("Unable to retrieve data",e);
        }
        
    }
    
     
     
}

