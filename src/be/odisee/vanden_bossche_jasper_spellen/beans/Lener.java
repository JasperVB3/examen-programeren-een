/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.odisee.vanden_bossche_jasper_spellen.beans;

/**
 *
 * @author JVDBG19
 */
public class Lener {
    private String lenernaam;
    private String gemeente;    
    private int spelnr;
    private int nr;
    private String telefoon;
    private String email;
   

    public Lener() {
    }

    public String getTelefoon() {
        return telefoon;
    }

    public void setTelefoon(String telefoon) {
        this.telefoon = telefoon;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getNr() {
        return nr;
    }

    public void setNr(int nr) {
        this.nr = nr;
    }

    public int getSpelnr() {
        return spelnr;
    }

    public void setSpelnr(int spelnr) {
        this.spelnr = spelnr;
    }

    public String getLenernaam() {
        return lenernaam;
    }

    public void setLenernaam(String lenernaam) {
        this.lenernaam = lenernaam;
    }

    public String getGemeente() {
        return gemeente;
    }

    public void setGemeente(String gemeente) {
        this.gemeente = gemeente;
    }

    
    

    @Override
    // case 3
    public String toString() {
        return "----------------------------------------" +"\n"+ "lenernaam: " + lenernaam + "\n" + "gemeente: " + gemeente +"\n"+ "----------------------------------------";
    }
    // case 7 toon alle leners
    public String toString2() {
        return " naam: " + lenernaam + "\n" + "---------------------------";
    
}
    //case 11 toon alle leners.
    public String toString4() {
        return "naam: " + lenernaam + "\n" + "nr: " + nr + "\n" + "---------------------------------------------------------------";
    }
    
    public String naam()
    {
        return lenernaam;
    }
}
