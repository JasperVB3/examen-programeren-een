/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.odisee.vanden_bossche_jasper_spellen.beans;

/**
 *
 * @author JVDBG19
 */
public class Moeilijkheid {
    private int nr;
    private String moeilijkheidnaam;
    Spel spel;

    public Moeilijkheid() {
    }

    public Spel getSpel() {
        return spel;
    }

    public void setSpel(Spel spel) {
        this.spel = spel;
    }

    public int getNr() {
        return nr;
    }

    public void setNr(int nr) {
        this.nr = nr;
    }

    public String getMoeilijkheidnaam() {
        return moeilijkheidnaam;
    }

    public void setMoeilijkheidnaam(String moeilijkheidnaam) {
        this.moeilijkheidnaam = moeilijkheidnaam;
    }

    @Override
    // case 8 moeilijkheden tonen
    public String toString() {
        return " moeilijkheidnaam: " + moeilijkheidnaam + "\n"+"---------------------------------------";
    }
    // case 8 spelen tonen met bepaalde moeilijkheidsgraad
    public String toString2() {
        return spel.getNaam()+ "\n" + "-------------------------------";
    }
    
    
}
