/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.odisee.vanden_bossche_jasper_spellen.beans;

import java.sql.Date;

/**
 *
 * @author JVDBG19
 */
public class Uitlening {
    private Date uitleendatum;
    private Date terugbrengdatum;
    private int nr;
    private int spelnr;
    private int lenernr;
    Spel spel;
    Lener lener;

    public Uitlening() {
    }

    public int getLenernr() {
        return lenernr;
    }

    public void setLenernr(int lenernr) {
        this.lenernr = lenernr;
    }

    public Date getUitleendatum() {
        return uitleendatum;
    }

    public void setUitleendatum(Date uitleendatum) {
        this.uitleendatum = uitleendatum;
    }

    public Date getTerugbrengdatum() {
        return terugbrengdatum;
    }

    public void setTerugbrengdatum(Date terugbrengdatum) {
        this.terugbrengdatum = terugbrengdatum;
    }

    

    public int getNr() {
        return nr;
    }

    public void setNr(int nr) {
        this.nr = nr;
    }

    public int getSpelnr() {
        return spelnr;
    }

    public void setSpelnr(int spelnr) {
        this.spelnr = spelnr;
    }

    public Spel getSpel() {
        return spel;
    }

    public void setSpel(Spel spel) {
        this.spel = spel;
    }

    public Lener getLener() {
        return lener;
    }

    public void setLener(Lener lener) {
        this.lener = lener;
    }

   

    @Override
    // case 3
    public String toString() {
        return "------------------------------"+"\n" + "uitleendatum=" + uitleendatum + ", terugbrengdatum=" + terugbrengdatum + "\n"+"------------------------------";
    }
    // case 11 laatste output toegevoegde lening (datums). Case gebruikt bij infolening
    public String toString3() {
        return "uitleendatum: " + uitleendatum + "\n" + "terugbrengdatum: " + terugbrengdatum + "\n"+"------------------------------------------------------------";
    }
}
