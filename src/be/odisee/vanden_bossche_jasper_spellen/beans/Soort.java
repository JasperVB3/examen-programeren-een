/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.odisee.vanden_bossche_jasper_spellen.beans;

/**
 *
 * @author JVDBG19
 */
public class Soort  {
    private int nr;
    private String soortnaam;
    

    public Soort() {
    }

    public int getNr() {
        return nr;
    }

    public void setNr(int nr) {
        this.nr = nr;
    }

    public String getSoortnaam() {
        return soortnaam;
    }

    public void setSoortnaam(String soortnaam) {
        this.soortnaam = soortnaam;
    }

    @Override
    // case 1
    public String toString() {
        return "--------------------------------"+"\n"+"nr: " + nr + "\n"+ "soortnaam: " + soortnaam + "\n" + "--------------------------------";
    }
    
            
            

   
    
    
}
