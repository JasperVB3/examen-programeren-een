/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.odisee.vanden_bossche_jasper_spellen.beans;
/**
 *
 * @author JVDBG19
 */
public class Spel {
    private int nr;
    private String naam;
    private String uitgever;
    private String auteur;
    private int jaar_uitgifte;
    private String leeftijd;
    private int min_spelers;
    private int max_spelers;
    private int soortnr;
    private String speelduur;
    private int moeilijkheidnr;
    private double prijs;
    private String afbeelding;
    Soort soort;
    Moeilijkheid moeilijkheid;
    Lener lener;
    Uitlening uitlening;

    
    
    
    public Spel() {
    }

    public Spel(String naam, String uitgever, double prijs) {
        this.naam = naam;
        this.uitgever = uitgever;
        this.prijs = prijs;
    }
    
    public Uitlening getUitlening() {
        return uitlening;
    }

    public void setUitlening(Uitlening uitlening) {
        this.uitlening = uitlening;
    }

    public Lener getLener() {
        return lener;
    }

    public void setLener(Lener lener) {
        this.lener = lener;
    }
    
    public Soort getSoort() {
        return soort;
    }

    public void setSoort(Soort soort) {
        this.soort = soort;
    }

    public Moeilijkheid getMoeilijkheid() {
        return moeilijkheid;
    }

    public void setMoeilijkheid(Moeilijkheid moeilijkheid) {
        this.moeilijkheid = moeilijkheid;
    }

    
    public int getNr() {
        return nr;
    }

    public void setNr(int nr) {
        this.nr = nr;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public String getUitgever() {
        return uitgever;
    }

    public void setUitgever(String uitgever) {
        this.uitgever = uitgever;
    }

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public int getJaar_uitgifte() {
        return jaar_uitgifte;
    }

    public void setJaar_uitgifte(int jaar_uitgifte) {
        this.jaar_uitgifte = jaar_uitgifte;
    }

    public String getLeeftijd() {
        return leeftijd;
    }

    public void setLeeftijd(String leeftijd) {
        this.leeftijd = leeftijd;
    }

    public int getMin_spelers() {
        return min_spelers;
    }

    public void setMin_spelers(int min_spelers) {
        this.min_spelers = min_spelers;
    }

    public int getMax_spelers() {
        return max_spelers;
    }

    public void setMax_spelers(int max_spelers) {
        this.max_spelers = max_spelers;
    }

    public int getSoortnr() {
        return soortnr;
    }

    public void setSoortnr(int soortnr) {
        this.soortnr = soortnr;
    }

    public String getSpeelduur() {
        return speelduur;
    }

    public void setSpeelduur(String speelduur) {
        this.speelduur = speelduur;
    }

    public int getMoeilijkheidnr() {
        return moeilijkheidnr;
    }

    public void setMoeilijkheidnr(int moeilijkheidnr) {
        this.moeilijkheidnr = moeilijkheidnr;
    }

    public double getPrijs() {
        return prijs;
    }

    public void setPrijs(double prijs) {
        this.prijs = prijs;
    }

    public String getAfbeelding() {
        return afbeelding;
    }

    public void setAfbeelding(String afbeelding) {
        this.afbeelding = afbeelding;
    }

    @Override
    //case 2 spel tonen en case 4 opgevraagd spel tonen
    public String toString() {
        return "------------------------------"+"\n"+"naam: " + naam + "\n"+ "uitgever: " + uitgever + "\n" + "leeftijd: " + leeftijd + "\n" + "prijs: " + prijs +"euro " + "\n" + "afbeelding: " + afbeelding + "\n"+ "------------------------------";
    }
    // case 5 alle spelen tonen.
     public String toString2() {
         return " naam: " + naam + "\n" + " uitgever " + uitgever + "\n" + " prijs " + prijs + "\n"+"---------------------------------------------------------";
     }
    // case 6 toon het geselecteerd spel
    public String toString3() {
        return "------------------------------Spel:-------------------------- "
                + "\n"+ " nr: " + nr +"\n"+ " naam " + naam + "\n"+ " uitgever: " + uitgever +"\n"+ " auteur: " + auteur +"\n"+ " jaar_uitgifte: " + jaar_uitgifte +"\n"+ " leeftijd: " + leeftijd +"\n"+ " min_spelers: " + min_spelers +"\n"+ " max_spelers: " + max_spelers +"\n"+ " soortnr: " + soortnr + "\n"+" speelduur: " + speelduur +"\n"+ " moeilijkheidnr: " + moeilijkheidnr +"\n"+ " prijs: " + prijs +"\n"+ " afbeelding: " + afbeelding + "\n" 
                +"----------------------------Soort:------------------------- "
                +"\n" + soort.getSoortnaam() + "\n"
                + "---------------------------Moeilijkheid:----------------------"
                +"\n"+moeilijkheid.getMoeilijkheidnaam()+ "\n" + "--------------------------------------------------------------";
    }
    // case 6 toon alle spelen.
    public String toString4() {
        return "naam: " + naam + "\n"+"soort: " + soort.getSoortnaam()+"\n"+"-------------------------------------------------";
    }
    //case 11 toon alle spellen
    public String toString5() {
        return "nr: " + nr + "\n" + "naam: " + naam + "\n" + "-----------------------------------------";
    }
    // case 11 infotmatie over het geselcteerde spel
    public String toString7() {
        return "nr: " + nr + "\n" + "naam: " + naam + "\n" +"leeftijd: " + leeftijd + "\n" + "min_spelers: " + min_spelers + "\n" + "max_spelers: " + max_spelers + "\n" + "speelduur: " + speelduur + "\n" + "prijs: " + prijs + "\n" + "afbeelding: " + afbeelding + "\n" + "----------------------------------------------";
    }
    // case 11 laatste output
    public String toString8()
    {
        return "lener nummer: " + uitlening.getLenernr()+ "\n"+"Lenernaam: " + lener.naam() + "\n" + "spel nummer: " +uitlening.getSpelnr() + "\n" + "Spelnaam: " + naam +"\n" +uitlening.toString3();
    }
    
    

   

   
    
    
}
