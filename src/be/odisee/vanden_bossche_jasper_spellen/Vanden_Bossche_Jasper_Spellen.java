/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.odisee.vanden_bossche_jasper_spellen;

import be.odisee.vanden_bossche_jasper_spellen.beans.Lener;
import be.odisee.vanden_bossche_jasper_spellen.beans.Moeilijkheid;
import be.odisee.vanden_bossche_jasper_spellen.beans.Soort;
import be.odisee.vanden_bossche_jasper_spellen.beans.Spel;
import be.odisee.vanden_bossche_jasper_spellen.beans.Uitlening;
import be.odisee.vanden_bossche_jasper_spellen.dataaccess.DALener;
import be.odisee.vanden_bossche_jasper_spellen.dataaccess.DAMoeilijkheid;
import be.odisee.vanden_bossche_jasper_spellen.dataaccess.DASoort;
import be.odisee.vanden_bossche_jasper_spellen.dataaccess.DASpel;
import be.odisee.vanden_bossche_jasper_spellen.dataaccess.DAUitlening;
import be.odisee.vanden_bossche_jasper_spellen.dataaccess.DAUitleningAdministratie;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author JVDBG19
 */
public class Vanden_Bossche_Jasper_Spellen {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String input;
        System.out.println("----------------------");
        System.out.println(" Vanden Bossche Jasper");
        System.out.println("");
        System.out.println("  Het SpelenHoekje");
        System.out.println("----------------------");
        
        int invoer = 0;
      
        do {
            System.out.println("Wat wil doen?");
            System.out.println("1. Spelsoorten");
            System.out.println("2. Spel");
            System.out.println("3. Lener");
            System.out.println("4. Toon een spel naar keuze");
            System.out.println("5. Toon alle spelen");
            System.out.println("6. Toon een lijst met spellen en kies een spel");
            System.out.println("7. toon uitlening");
            System.out.println("8. Uitgebreid zoeken: moeilijkheid");
            System.out.println("9. Uitgebreid zoeken: leners");
            System.out.println("10. zoeken in uitleningen");
            System.out.println("11. Administratie Uitleningen");
            System.out.println("12. Exit");
        System.out.println("Geef uw keuze in aub");
        Scanner scanner = new Scanner(System.in);
        while (!scanner.hasNextInt()) {
            String inputtest = scanner.next();
            System.out.println(inputtest + " is geen correcte invoer, kies een nummer uit de lijst: ");
            System.out.println("Wat wil doen?");
            System.out.println("1. Spelsoorten");
            System.out.println("2. Spel");
            System.out.println("3. Lener");
            System.out.println("4. Toon een spel naar keuze");
            System.out.println("5. Toon alle spelen");
            System.out.println("6. Toon een lijst met spellen en kies een spel");
            System.out.println("7. toon uitlening");
            System.out.println("8. Uitgebreid zoeken: moeilijkheid");
            System.out.println("9. Uitgebreid zoeken: leners");
            System.out.println("10. zoeken in uitleningen");
            System.out.println("11. Administratie Uitleningen");
            System.out.println("12. Exit");
        }
        invoer = scanner.nextInt();
        
        switch (invoer){
            case 1: 
                    System.out.println("spelsoorten");
                
                DASoort soort = new DASoort();
                Soort keuze1 = soort.findSoort();
                    System.out.println(keuze1);
            break;
            
            case 2:
                    System.out.println("spel");
                
                DASpel spel = new DASpel();
                Spel keuze2 = spel.findSpel();
                    System.out.println(keuze2);
            break;
               
            case 3:
                    System.out.println("uitlening");
                
                DALener lener = new DALener();
                Lener keuze3 = lener.findLener();
                    System.out.println(keuze3);
            break;
            
            case 4:
                    System.out.println("Toon een spel naar keuze");
                
                String userInput = scanner.next();
                boolean try3 = true;
                
                while (try3 == true){
                DASpel daspel = new DASpel();
                Spel pspel = daspel.searchSpel(userInput.toLowerCase());
                if (pspel == null){
                    System.out.println("-Zoekopdracht niet gevonden.-");
                    System.out.println("-Geef een bestaand spel in.-");
                    System.out.println(userInput = scanner.next());
                    
                }else{
                    try3 = false;
                    System.out.println(pspel);
                }}
                
               
            break;
            case 5:
                        
                    System.out.println("Toon alle spelen.");
                  DASpel daAllSpellen = new DASpel();
                  ArrayList <Spel> spellen = daAllSpellen.allSpellen();
                  System.out.println("---------------------------------------------------------");
                  for (Spel spel2 : spellen){
                    System.out.println(spel2.toString2());
                  
                  
                  }
                  break;
            
            case 6:
                    System.out.println("Toon een lijst met spelen en kies een spel");
                
                  DASpel daAllSpellen2 = new DASpel();
                  ArrayList <Spel> spellen2 = daAllSpellen2.allSpellen2();
                  
                  for (Spel spel2 : spellen2){
                    System.out.println(spel2.toString4());
                  }
                    System.out.println("Kies een spel.");
                String userInput2 = scanner.next();
                boolean try2 = true;
                
                while (try2 == true){
                DASpel daspel2 = new DASpel();
                Spel pspel2 = daspel2.searchSpel2(userInput2.toLowerCase());
                
                if (pspel2 == null){
                    System.out.println("-Zoekopdracht niet gevonden.-");
                    System.out.println("-Geen correcte invoer. Geef een bestaand spel in.-");
                    userInput2 = scanner.next();
                }else{
                    try2 = false;
                    System.out.println(pspel2.toString3());
                }}
            break;
            case 7:
                DAUitlening daAllUitleningen = new DAUitlening();
                ArrayList <Uitlening> uitleningen = daAllUitleningen.allUitleningen();
                
                    System.out.println("------------------------------------------------------------------------------------------------------------------------------------------------");
                String kolommen = "%-10s %35s %44s %36s%n";
                String positieKolommen = "%1$-40s %2$-40s %3$-30s %4$-30s%n";
                    System.out.printf(kolommen,"spelnaam", "lener", "uitlening", "terugbrengdatum");
                    System.out.println("------------------------------------------------------------------------------------------------------------------------------------------------");
                
                  
                for (Uitlening uitlening1 : uitleningen){
                    System.out.printf(positieKolommen, uitlening1.getSpel().getNaam(), uitlening1.getLener().getLenernaam(),uitlening1.getUitleendatum(), uitlening1.getTerugbrengdatum());
                  }
                    System.out.println(" ");
                
                boolean try8 = true;
                while (try8 == true) {
                    System.out.println("Geef 1 in om verder te gaan naar de lijst met leners." + "\n" + "Geef 2 in om uit het programma te gaan.");
                int userInput10 = scanner.nextInt();
                
                if (userInput10 == 1){
                    System.out.println("Lijst met leners");
                DALener daAllLener = new DALener();
                ArrayList <Lener> leners = daAllLener.allLener();
                  
                for (Lener lener1 : leners){
                    System.out.println(lener1.toString2());
                  }
                
                
                    System.out.println("Geef de naam van de lener in.");
                String userInput3 = scanner.next();
                boolean try1 = true;
                while (try1 == true){
                    try8 = false;
                DAUitlening dauitlening3 = new DAUitlening();
                ArrayList <Uitlening> puitlening = dauitlening3.allUitleningen2(userInput3.toLowerCase());
                
               if (!puitlening.isEmpty()){
                   try1 = false;
                    System.out.println("------------------------------------------------------------------------------------------------------------------------------------------------");
                String kolommen2 = "%-10s %35s %44s %36s%n";
                String positieKolommen2 = "%1$-40s %2$-40s %3$-30s %4$-30s%n";
                    System.out.printf(kolommen2,"spelnaam", "lener", "uitlening", "terugbrengdatum");
                    System.out.println("------------------------------------------------------------------------------------------------------------------------------------------------");
                
                
                 for (Uitlening uitlening3 : puitlening){
                    System.out.printf(positieKolommen2, uitlening3.getSpel().getNaam(), uitlening3.getLener().getLenernaam(),uitlening3.getUitleendatum(), uitlening3.getTerugbrengdatum());
                  }
                    System.out.println(" ");
               }
                else {
                    System.out.println("-Zoekopdracht niet gevonden-");
                    System.out.println("-Persoon heeft geen leningen of staat niet in de database.-");
                    userInput3 = scanner.next();
                }}
                }
                if ( userInput10 == 2) {
                    System.exit(0);   
                }else {
                    System.out.println("-Ongeldige invoer.-");
                    
                }}
                 
                 
                 break;
            case 8:
                    System.out.println("Moeilijkheden");
                    System.out.println("---------------------------------------");
                DAMoeilijkheid daAllmoeilijkheid = new DAMoeilijkheid();
                ArrayList <Moeilijkheid> pmoeilijkheid = daAllmoeilijkheid.allMoeilijkheid();
                  
                for (Moeilijkheid moeilijkheid1 : pmoeilijkheid){
                    System.out.println(moeilijkheid1);
                  }
                    System.out.println("Kies een moeilijkheid 1 tot 5.");
                     while (!scanner.hasNextInt()) {
                    input = scanner.next();
                    System.out.println("-De invoer was geen nummer. Probeer opnieuw.-");
                        }
                int userInput4 = scanner.nextInt();
                boolean try6 = true;
                while (try6 == true){
                DAMoeilijkheid damoeilijkheid = new DAMoeilijkheid();
                ArrayList <Moeilijkheid> moeilijkheden = damoeilijkheid.allMoeilijkheid2(userInput4);
                if (!moeilijkheden.isEmpty()){
                try6 = false;
                for (Moeilijkheid moeilijkheid1 : moeilijkheden){
                    System.out.println(moeilijkheid1.toString2());
                  }
                }else {
                    System.out.println("-Zoekopdracht niet gevonden.-");
                    System.out.println("-Geef een nummer van 1-5 in.-");
                    userInput4 = scanner.nextInt();
                }}
                
            break;
            case 9:
                    System.out.println("Uitgebreid zoeken: leners");
                    System.out.println("Geef een Lener in.");
                
                String userInput1 = scanner.next();
                boolean try7 = true;
                while (try7 == true) {
                DALener dalener = new DALener();
                ArrayList <Lener> plener = dalener.allLener2(userInput1.toLowerCase());
                
               if (!plener.isEmpty()){
                try7 = false;
                    System.out.println("------------------------------------------------------------------------------------------------------------------------------------------------");
                String kolommen2 = "%-10s %35s %44s %36s%n";
                String positieKolommen2 = "%1$-40s %2$-40s %3$-30s %4$-30s%n";
                    System.out.printf(kolommen2,"naam", "gemeente", "telefoon", "e-mailadres");
                    System.out.println("------------------------------------------------------------------------------------------------------------------------------------------------");
                
                
                 for (Lener lener1 : plener){
                    System.out.printf(positieKolommen2, lener1.getLenernaam(), lener1.getGemeente(), lener1.getTelefoon(), lener1.getEmail() );
                  }
                    System.out.println(" ");
               }
                else {
                    System.out.println("-Zoekopdracht niet gevonden.-");
                    System.out.println("-Geef een bestaande lener in.-");
                    userInput1 = scanner.next();
                }}
            break;
            case 10:
                DAUitlening daAllUitleningen3 = new DAUitlening();
                ArrayList <Uitlening> uitleningen2 = daAllUitleningen3.allUitleningen3();
                System.out.println("Openstaande leningen.");
                    System.out.println("------------------------------------------------------------------------------------------------------------------------------------------------");
                String kolommen2 = "%-10s %35s %44s %36s%n";
                String positieKolommen2 = "%1$-40s %2$-40s %3$-30s %4$-30s%n";
                    System.out.printf(kolommen2,"spelnaam", "lener", "uitlening", "terugbrengdatum");
                    System.out.println("------------------------------------------------------------------------------------------------------------------------------------------------");
                
                  
                for (Uitlening uitlening1 : uitleningen2){
                    System.out.printf(positieKolommen2, uitlening1.getSpel().getNaam(), uitlening1.getLener().getLenernaam(),uitlening1.getUitleendatum(), uitlening1.getTerugbrengdatum());
                  }
                    System.out.println(" ");
            
            break;
            case 11:
                    System.out.println("Toon alle spelen.");
                DASpel daAllSpellen3 = new DASpel();
                ArrayList <Spel> spellen1 = daAllSpellen3.allSpellen3();
                  
                for (Spel spel2 : spellen1){
                    System.out.println(spel2.toString5());
                  }
                  
                    System.out.println("Geef het nummer van het spel dat u wilt selecteren in.");
                
                while (!scanner.hasNextInt()) {
                    input = scanner.next();
                    System.out.println("-De invoer was geen nummer. Probeer opnieuw.-");
                        }
                
                int userInput5 = scanner.nextInt();
                boolean try4 = true;
                while(try4 ==true){
                    DASpel daspel3 = new DASpel();
                    Spel pspel3 = daspel3.searchSpel3(userInput5);
                
                if (pspel3 == null) {
                    
                    System.out.println("-Zoekopdracht niet gevonden.-");
                    System.out.println(" ");
                    userInput5 = scanner.nextInt();
                }else{
                    try4 =false;
                    System.out.println(pspel3.toString7());
                }
                }
                    System.out.println("Leners");
                    DALener dalener2 = new DALener();
                    ArrayList <Lener> leners2 = dalener2.allLener();
                
                for (Lener lener2 : leners2) {
                    System.out.println(lener2.toString4());
                }
                
                    System.out.println("Geef het nummer van de lener waar je een uitlening wilt aan toevoegen:");
                while (!scanner.hasNextInt()) {
                    input = scanner.next();
                    System.out.println("-De invoer was geen nummer. Probeer opnieuw.-");
                        }
        int userInput6 = scanner.nextInt();
        boolean try5 = true;
        while(try5 == true)
        {
            DALener lener4 = new DALener();
            Lener lener9 = lener4.searchLener(userInput6);
            if(lener9 == null)
            {
                System.out.println("-Geen lener gevonden.-");
                userInput6 = scanner.nextInt();
            }
            else
            {
                try5 =false;
                System.out.println("------------------------------------------------------------");
            }
        }
        
        DAUitleningAdministratie uitlening = new DAUitleningAdministratie();
        Uitlening puitleningadministratie = uitlening.addUitleningAdministratie(userInput5,userInput6);
        
                DAUitlening daAllUitleningen4 = new DAUitlening();
                ArrayList <Spel> uitleningen5 = daAllUitleningen4.allUitleningen4();
                for(Spel leners8: uitleningen5)
                {
                    System.out.println(leners8.toString8());
                }
        
            System.exit(0);      
            break;
            
            
            
            
            
            case 12:
                System.out.println("Exit");
            break; 
            default:
                System.out.println("-Zoekopdracht niet gevonden.-");
                System.out.println(" ");
        }   
        
      }
           while (invoer != 12);
        }
      }
